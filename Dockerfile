FROM ruby:2.6.5-alpine

ENV \
  DEV_PACKAGES="bash ruby-dev build-base postgresql-dev tzdata git nodejs yarn" \
  IMAGE_MAGICK_PACKAGES="optipng jpegoptim pacman imagemagick imagemagick-dev" \
  APP_HOME="/app"

WORKDIR $APP_HOME

RUN \
  rm -rf /app && \
  mkdir -p /app/tmp/pids /app/tmp/cache /app/tmp/sockets

# Установка зависимостей
RUN set -ex && \
  apk --update --upgrade add --no-cache $DEV_PACKAGES && \
  apk --update --upgrade add --no-cache $IMAGE_MAGICK_PACKAGES && \
  rm -rf /var/cache/apk/* && \
  echo ‘gem: --no-document’ > /etc/gemrc

# Установка гемов
RUN gem install bundler

# Установка часового пояса
RUN cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    echo "Europe/Moscow" > /etc/timezone

# Установка гемов
COPY Gemfile* ./
RUN bundle install
# RUN bundle install --without development test

# Установка пакетов из веб пака
COPY package.json ./
COPY yarn.lock ./
RUN yarn install --check-files
